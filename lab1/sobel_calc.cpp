#include "opencv2/imgproc/imgproc.hpp"
#include "sobel_alg.h"
using namespace cv;

/*******************************************
 * Model: grayScale
 * Input: Mat img
 * Output: None directly. Modifies a ref parameter img_gray_out
 * Desc: This module converts the image to grayscale
 ********************************************/
void grayScale(Mat& img, Mat& img_gray_out)
{
	uint8_t color, color2;
	uint8_t r,g,b,r2,g2,b2;
	for(int i = 0; i < IMG_WIDTH * IMG_HEIGHT * 3; i += 6){
	  r = ((uint8_t)29 * (uint16_t)img.data[i]) >> 8;
	  g = ((uint8_t)150 * (uint16_t)img.data[i+1]) >> 8;
	  b = ((uint8_t)77 * (uint16_t)img.data[i+2]) >> 8;
	  color = r + g + b; //Convert coefficients for RGB to fixed point 
	  img_gray_out.data[i/3] = color;

	  r2 = ((uint8_t)29 * (uint16_t)img.data[i+3]) >> 8;
	  g2 = ((uint8_t)150 * (uint16_t)img.data[i+4]) >> 8;
	  b2 = ((uint8_t)77 * (uint16_t)img.data[i+5]) >> 8;
	  color2 = r2 + g2 + b2;
	  img_gray_out.data[i/3 + 1] = color2;
	}
}

void grayScaleFH(Mat& img, Mat& img_gray_out){
  uint8_t color, color2;
  uint8_t r,g,b,r2,g2,b2;
  for(int i = 0; i < ((IMG_WIDTH * IMG_HEIGHT) * 3)/2; i += 6){
    r = ((uint8_t)29 * (uint16_t)img.data[i]) >> 8;
    g = ((uint8_t)150 * (uint16_t)img.data[i+1]) >> 8;
    b = ((uint8_t)77 * (uint16_t)img.data[i+2]) >> 8;
    color = r + g + b; //Convert coefficients for RGB to fixed point 
    img_gray_out.data[i/3] = color;

    r2 = ((uint8_t)29 * (uint16_t)img.data[i+3]) >> 8;
    g2 = ((uint8_t)150 * (uint16_t)img.data[i+4]) >> 8;
    b2 = ((uint8_t)77 * (uint16_t)img.data[i+5]) >> 8;
    color2 = r2 + g2 + b2;
    img_gray_out.data[i/3 + 1] = color2;

  }
}

void grayScaleSH(Mat& img, Mat& img_gray_out){
  uint8_t color, color2;
  uint8_t r,g,b,r2,g2,b2;
  for(int i = ((IMG_WIDTH*IMG_HEIGHT)*3)/2; i < (IMG_WIDTH * IMG_HEIGHT ) * 3; i += 6){
    r = ((uint8_t)29 * (uint16_t)img.data[i]) >> 8;
    g = ((uint8_t)150 * (uint16_t)img.data[i+1]) >> 8;
    b = ((uint8_t)77 * (uint16_t)img.data[i+2]) >> 8;
    color = r + g + b; //Convert coefficients for RGB to fixed point 
    img_gray_out.data[i/3] = color;
    r2 = ((uint8_t)29 * (uint16_t)img.data[i+3]) >> 8;
    g2 = ((uint8_t)150 * (uint16_t)img.data[i+4]) >> 8;
    b2 = ((uint8_t)77 * (uint16_t)img.data[i+5]) >> 8;
    color2 = r2 + g2 + b2;
    img_gray_out.data[i/3 + 1] = color2;
  }
}

/*******************************************
 * Model: sobelCalc
 * Input: Mat img_in
 * Output: None directly. Modifies a ref parameter img_sobel_out
 * Desc: This module performs a sobel calculation on an image. It first
 *  converts the image to grayscale, calculates the gradient in the x 
 *  direction, calculates the gradient in the y direction and sum it with Gx
 *  to finish the Sobel calculation
 ********************************************/
void sobelCalc(Mat& img_gray, Mat& img_sobel_out)
{
  Mat img_outx = img_gray.clone();
  Mat img_outy = img_gray.clone();
  unsigned short sobel;
  for(int i=1; i < img_gray.rows-1; i++){
    for(int j=1; j < img_gray.cols-2; j+=2){ //-2 for loop unraveling
      uint16_t a1, a2;
      uint16_t b1, b2;
      uint16_t c1, c2;
      a1 = img_gray.data[IMG_WIDTH*(i-1) + (j-1)];
      a2 = img_gray.data[IMG_WIDTH*(i-1) + j];
      b1 = img_gray.data[IMG_WIDTH*(i) + (j-1)];
      b2 = img_gray.data[IMG_WIDTH*(i) + (j+1)];
      c1 = img_gray.data[IMG_WIDTH*(i+1) + j];
      c2 = img_gray.data[IMG_WIDTH*(i+1) + (j+1)];
      sobel = 2 * abs(a1 + a2 + b1 - b2 - c1 - c2);
      sobel = (sobel > 255) ? 255 : sobel;
      img_sobel_out.data[IMG_WIDTH*i + j] = sobel;
      //a1 = img_gray.data[IMG_WIDTH*(i-1) + (j)];
      a1 = img_gray.data[IMG_WIDTH*(i-1) + (j+1)];
      b1 = img_gray.data[IMG_WIDTH*(i) + j];
      b2 = img_gray.data[IMG_WIDTH*(i) + (j+2)];
      //c1 = img_gray.data[IMG_WIDTH*(i+1) + (j+1)];
      c1 = img_gray.data[IMG_WIDTH*(i+1) + (j+2)];
      sobel = 2 * abs(a1 + a2 + b1 - b2 - c1 - c2);
      sobel = (sobel > 255) ? 255 : sobel;
      img_sobel_out.data[IMG_WIDTH*i + j+1] = sobel;
    }
  }
}


void sobelCalcFH(Mat &img_gray, Mat& img_sobel_out){
  Mat img_outx = img_gray.clone();
  Mat img_outy = img_gray.clone();
  unsigned short sobel;
  for(int i=1; i < (img_gray.rows-1)/2; i++){
    for(int j=1; j < (img_gray.cols-1); j++){ //no loop unravelling
      uint16_t a1, a2;
      uint16_t b1, b2;
      uint16_t c1, c2;
      a1 = img_gray.data[IMG_WIDTH*(i-1) + (j-1)];
      a2 = img_gray.data[IMG_WIDTH*(i-1) + j];
      b1 = img_gray.data[IMG_WIDTH*(i) + (j-1)];
      b2 = img_gray.data[IMG_WIDTH*(i) + (j+1)];
      c1 = img_gray.data[IMG_WIDTH*(i+1) + j];
      c2 = img_gray.data[IMG_WIDTH*(i+1) + (j+1)];
      sobel = 2 * abs(a1 + a2 + b1 - b2 - c1 - c2);
      sobel = (sobel > 255) ? 255 : sobel;
      img_sobel_out.data[IMG_WIDTH*i + j] = sobel;
    }
  }
}

void sobelCalcSH(Mat &img_gray, Mat& img_sobel_out){
  Mat img_outx = img_gray.clone();
  Mat img_outy = img_gray.clone();
  unsigned short sobel;
  for(int i=(img_gray.rows-1)/2; i < (img_gray.rows-1); i++){
    for(int j=1; j < (img_gray.cols-1); j++){ //no loop unravelling
      uint16_t a1, a2;
      uint16_t b1, b2;
      uint16_t c1, c2;
      a1 = img_gray.data[IMG_WIDTH*(i-1) + (j-1)];
      a2 = img_gray.data[IMG_WIDTH*(i-1) + j];
      b1 = img_gray.data[IMG_WIDTH*(i) + (j-1)];
      b2 = img_gray.data[IMG_WIDTH*(i) + (j+1)];
      c1 = img_gray.data[IMG_WIDTH*(i+1) + j];
      c2 = img_gray.data[IMG_WIDTH*(i+1) + (j+1)];
      sobel = 2 * abs(a1 + a2 + b1 - b2 - c1 - c2);
      sobel = (sobel > 255) ? 255 : sobel;
      img_sobel_out.data[IMG_WIDTH*i + j] = sobel;
    }
  }
}
